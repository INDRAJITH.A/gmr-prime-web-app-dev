import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { Location } from '@angular/common';
import {setAnimation} from "../../../assets/js/gmr.js";
import { AngularFireDatabase } from 'angularfire2/database';
import { GlobalService } from '../../providers/global.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
declare var bootbox: any;

@Component({
  selector: 'app-booking-service',
  templateUrl: './booking-service.component.html',
  styleUrls: ['./booking-service.component.css']
})
export class BookingServiceComponent implements OnInit {

  serviceName: any;
  adultsCount: any;
  childsCount: any;
  infantCount: any;
  itemsOnCart: any;
  addonsOnCart: any;
  portersOnCart: any;
  chargeDetails: any;
  placardName: any;
  placardMessage: any;
  packageList: any;

  d2D: any;
  d2I: any;
  i2D: any;

  adultsCountArray: any;
  childsCountArray: any;
  infantsCountArray:any;

  step1:boolean;
  step2:boolean;
  step3:boolean;
  //to show step3 active class
  showstep3:boolean;
  showplacard: boolean;
  showpayment: boolean;

  guestForm: FormGroup;
  infantMindate: any;
  infantMaxdate: any;
  childMindate: any;
  childMaxdate: any;
  adultMindate: any;
  adultMaxdate: any;

  departTravelType: any;
  departAirLines: any; 
  departFlightNo: any; 
  departDestination: any; 
  departureDate: any;
  departFlightTime: any; 
  departServiceTime: any; 

  arrivalTravelType: any; 
  arrivalAirLines: any; 
  arrivalFlightNo: any; 
  arrivalDestination: any; 
  arrivalDate: any; 
  arrivalFlightTime: any; 
  arrivalServiceTime: any;

  isOneTimeGoldItemAdded: boolean = false;
  isOneTimePlatinumItemAdded: boolean = false;
  isPrevGoldItemAdded: boolean = false;
  isPrevPlatinumItemAdded: boolean = false;
  isPortItemAdded: boolean = false;
  isLoungeItemAdded: boolean = false;
  isServiceAdded: boolean = false;
  addOnServices: any;
  oneTimePackage: any;
  privilegePackage: any;

  isD2DGoldItemAdded: boolean = false;
  isD2DPlatinumItemAdded: boolean = false;
  isD2IGoldItemAdded: boolean = false;
  isD2IPlatinumItemAdded: boolean = false;
  isI2DGoldItemAdded: boolean = false;
  isI2DPlatinumItemAdded: boolean = false;

  transferTravelType: any;

  constructor(
    public firebaseDB: AngularFireDatabase,
    public router: Router,
    public location: Location, 
    public global: GlobalService,
    public formBuilder: FormBuilder) { 
    }

  ngOnInit() {
    setAnimation();
    this.initalValueGST();
    this.removePackageFromCart();
    this.removePortersFromCart();
    this.removeAddonsFromCart();
    this.guestForm = this.formBuilder.group({
      
      adultDetails: this.formBuilder.array([]),
      childDetails: this.formBuilder.array([]),
      infantDetails: this.formBuilder.array([]),

    });
    this.dateOfBirthvalidation();
  }

  initForm() {

    return this.formBuilder.group({
      firstName: ['', Validators.compose([Validators.maxLength(20), Validators.minLength(3), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.maxLength(20), Validators.minLength(1), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      mobileNumber: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9 ]*'), Validators.required])],
      email: ['', Validators.compose([Validators.maxLength(70), Validators.minLength(10)])],
      national: ['', [Validators.required]],
      pnrNo: ['', [Validators.required, Validators.minLength(3)]],
      dob: ['', [Validators.required]],
      passenger: ['']


    });

  }


  adultFormDetails(n) {
    // console.log(n);
    for (var i = 0; i < n; i++) {
      const control = <FormArray>this.guestForm.controls['adultDetails'];
      control.push(this.initForm());
    }

  }
  childFormDetails(n) {
    // console.log(n);
    for (var i = 0; i < n; i++) {
      const control = <FormArray>this.guestForm.controls['childDetails'];
      control.push(this.initForm());
    }
  }
  infantFormDetails(n) {
    // console.log(n);
    for (var i = 0; i < n; i++) {
      const control = <FormArray>this.guestForm.controls['infantDetails'];
      control.push(this.initForm());
    }
  }

  dateOfBirthvalidation() {
    let newDateInfantmin: Date = new Date();
    let newDateInfantmax: Date = new Date();
    let newDateChildmin: Date = new Date();
    let newDateChildmax: Date = new Date();
    let newDateAdultmin: Date = new Date();
    let newDateAdultmax: Date = new Date();

    let infantMinimum = new Date(newDateInfantmin.setDate(newDateInfantmin.getDate() - (365 * 3)));
    this.infantMindate = new Date(infantMinimum.toISOString().split('T')[0]);
    let infantMaximum = new Date(newDateInfantmax.setDate(newDateInfantmax.getDate() - 1));
    this.infantMaxdate = new Date(infantMaximum.toISOString().split('T')[0]);

    let childMinimum = new Date(newDateChildmin.setDate(newDateChildmin.getDate() - (365 * 15)));
    this.childMindate = new Date(childMinimum.toISOString().split('T')[0]);
    let childMaximum = new Date(newDateChildmax.setDate(newDateChildmax.getDate() - ((365 * 3) - 1)));
    this.childMaxdate = new Date(childMaximum.toISOString().split('T')[0]);

    let adultMaximum = new Date(newDateAdultmax.setDate(newDateAdultmax.getDate() - ((365 * 15) - 1)));
    this.adultMaxdate = new Date(adultMaximum.toISOString().split('T')[0]);

    let adultMinimum = new Date(newDateAdultmin.setDate(newDateAdultmin.getDate() - (365 * 99)));
    this.adultMindate =new Date(adultMinimum.toISOString().split('T')[0]);
   
  }

  ngAfterContentInit(){
     this.departTravelType = this.global.getDepartTravelType();
     this.departAirLines = this.global.getDepartAirline();
     this.departFlightNo = this.global.getDepartFlightNo();
     this.departDestination = this.global.getDepartDestination();
     this.departureDate = this.global.getDepartFlightDate();
     this.departFlightTime = this.global.getDepartFlightTime();
     this.departServiceTime = this.global.getDepartServiceTime();

     this.arrivalTravelType = this.global.getArrivalTravelType();
     this.arrivalAirLines = this.global.getArrivalAirline();
     this.arrivalFlightNo = this.global.getArrivalFlightNo();
     this.arrivalDestination = this.global.getArrivalDestination();
     this.arrivalDate = this.global.getArrivalFlightDate();
     this.arrivalFlightTime = this.global.getArrivalFlightTime();
     this.arrivalServiceTime = this.global.getArrivalServiceTime();

     this.serviceName = this.global.getServiceName();
     this.adultsCount = this.global.getAdultCount();
     this.childsCount = this.global.getChildCount();
     this.infantCount = this.global.getInfantCount();

     this.transferTravelType = this.global.getTransferTravelType();

     this.getArrivalPackages();
     this.getDeparturePackages();
     this.getTransferPackages();
      //  Adult count //child count //infant count
    this.adultFormDetails(this.adultsCount);
    this.childFormDetails(this.childsCount);
    this.infantFormDetails(this.infantCount);

    // this.adultsCountArray = Array(+this.global.getAdultCount()).fill(this.adultsCount).map((x,i)=>i);
    // this.childsCountArray = Array(+this.global.getAdultCount()).fill(this.childsCount).map((x,i)=>i);
    // this.infantsCountArray = Array(+this.global.getInfantCount()).fill(this.infantCount).map((x,i)=>i);

    
    

    //show step1
    this.step1 = true;
    this.step2 = false;
    this.step3 = false;
    this.showstep3 = false;
    this.showplacard = false;
    this.showpayment = false;

  }

  getDeparturePackages(){
      var queryPath = '/serviceDetails/' + this.serviceName;
      this.firebaseDB.object(queryPath).valueChanges().subscribe(res => {
        this.addOnServices = Object.values(res["addOns"]);
        this.oneTimePackage = Object.values(res["oneTimePlan"].packages);
        this.privilegePackage = Object.values(res["privilegePlan"].packages);
    });
    
  }

  togglingOneTimePackage(index, items, valueChanges){
    this.isPrevGoldItemAdded = false;
    this.isPrevPlatinumItemAdded = false;

    if(index == 0 && valueChanges){
      this.addDepartureItemsToCart(items);
      this.isOneTimePlatinumItemAdded = false;
    }else if (index == 0 && !valueChanges){
      this.removePackageFromCart();
      this.isOneTimeGoldItemAdded = false;
    }else if(index == 1 && valueChanges){
      this.addDepartureItemsToCart(items);
      this.isOneTimeGoldItemAdded = false;
    }else if (index == 1 && !valueChanges){
      this.removePackageFromCart();
      this.isOneTimePlatinumItemAdded = false;
    }
    this.calculateGST();
  }


  togglingPreviliegePackage(index, items, valueChanges){
    this.isOneTimeGoldItemAdded = false;
    this.isOneTimePlatinumItemAdded = false;

    if(index == 0 && valueChanges){
      this.addDepartureItemsToCart(items);
      this.isPrevPlatinumItemAdded = false;
    }else if (index == 0 && !valueChanges){
      this.removePackageFromCart();
      this.isPrevGoldItemAdded = false;
    }else if(index == 1 && valueChanges){
      this.addDepartureItemsToCart(items);
      this.isPrevGoldItemAdded = false;
    }else if (index == 1 && !valueChanges){
      this.removePackageFromCart();
      this.isPrevPlatinumItemAdded = false;
    }
    this.calculateGST();
  }


  togglingPorterPackage(items, valueChanges){
    if(valueChanges){
      this.addDeparturePortersToCart(items);
    }else if (!valueChanges){
      this.removePortersFromCart();
    }

    this.calculateGST();
  }

  togglingAddonPackage(items, valueChanges){
    if(valueChanges){
      this.addDepartureAddonsToCart(items);
    }else if (!valueChanges){
      this.removeAddonsFromCart();
    }

    this.calculateGST();
  }


  addDeparturePortersToCart(values){
    this.portersOnCart = {
      "porters": values.name,
      "porter_amt": this.departTravelType == 'domestic' ? values.amount.domestic : values.amount.international
    }
  }


  addDepartureAddonsToCart(values){
    this.addonsOnCart = {
      "addons": values.name,
      "addon_amt": this.departTravelType == 'domestic' ? values.amount.domestic : values.amount.international
    }
  }


  addDepartureItemsToCart(values){
    this.itemsOnCart = {
      "package": values.pkgName,
      "pkg_amt": this.departTravelType == 'domestic' ? values.domesticAmt : values.internationalAmt,
    }
  }


  getArrivalPackages(){
    var queryPath = '/serviceDetails/' + this.serviceName + '/' + this.arrivalTravelType;
    this.firebaseDB.object(queryPath).valueChanges().subscribe(res => {
      this.packageList = Object.values(res["services"]);
    });
  }


  togglingServicePackage(items, valueChanges){
    if(valueChanges){
      this.isServiceAdded = true;
      this.addArrivalPackageToCart(items);
    }else if (!valueChanges){
      this.isServiceAdded = false;
      this.removePackageFromCart();
    }
    this.calculateGST();
  }


  togglingArrivalPorterPackage(items, valueChanges){
    if(valueChanges){
      this.addArrivalPorterToCart(items);
    }else if (!valueChanges){
      this.removePortersFromCart();
    }
    this.calculateGST();
  }


  removePackageFromCart(){
    this.itemsOnCart = {
      "package": "",
      "pkg_amt": 0
    }
  }

  removePortersFromCart(){
    this.portersOnCart = {
      "porters": "",
      "porter_amt": 0
    }
  }

  removeAddonsFromCart(){
    this.addonsOnCart = {
      "addons": "",
      "addon_amt": 0
    }
  }
 

  addArrivalPackageToCart(values){
    this.itemsOnCart = {
      "package": values.name,
      "pkg_amt": values.amount
    }
  }

  addArrivalPorterToCart(values){
    this.portersOnCart = {
      "porters": values.name,
      "porter_amt": values.amount
    }
  }

  getTransferPackages(){
    var queryPath = '/serviceDetails/' + this.serviceName;
    this.firebaseDB.object(queryPath).valueChanges().subscribe(res => {
      this.addOnServices = Object.values(res["addOns"]);
      this.d2D = Object.values(res["d2d"].packages);
      this.d2I = Object.values(res["d2i"].packages);
      this.i2D = Object.values(res["i2d"].packages);
  });
}

  togglingD2DPackage(index, items, valueChanges){
    this.isD2IGoldItemAdded = false;
    this.isD2IPlatinumItemAdded = false;
    this.isI2DGoldItemAdded = false;
    this.isI2DPlatinumItemAdded = false;

    if(index == 0 && valueChanges){
      this.addTransferItemsToCart(items);
      this.isD2DPlatinumItemAdded = false;
    }else if (index == 0 && !valueChanges){
      this.removePackageFromCart();
      this.isD2DGoldItemAdded = false;
    }else if(index == 1 && valueChanges){
      this.addTransferItemsToCart(items);
      this.isD2DGoldItemAdded = false;
    }else if (index == 1 && !valueChanges){
      this.removePackageFromCart();
      this.isD2DPlatinumItemAdded = false;
    }
    this.calculateGST();
  }


  togglingD2IPackage(index, items, valueChanges){
    this.isD2DGoldItemAdded = false;
    this.isD2DPlatinumItemAdded = false;
    this.isI2DGoldItemAdded = false;
    this.isI2DPlatinumItemAdded = false;

    if(index == 0 && valueChanges){
      this.addTransferItemsToCart(items);
      this.isD2IPlatinumItemAdded = false;
    }else if (index == 0 && !valueChanges){
      this.removePackageFromCart();
      this.isD2IGoldItemAdded = false;
    }else if(index == 1 && valueChanges){
      this.addTransferItemsToCart(items);
      this.isD2IGoldItemAdded = false;
    }else if (index == 1 && !valueChanges){
      this.removePackageFromCart();
      this.isD2IPlatinumItemAdded = false;
    }
    this.calculateGST();
  }

  togglingI2DPackage(index, items, valueChanges){
    this.isD2DGoldItemAdded = false;
    this.isD2DPlatinumItemAdded = false;
    this.isD2IGoldItemAdded = false;
    this.isD2IPlatinumItemAdded = false;

    if(index == 0 && valueChanges){
      this.addTransferItemsToCart(items);
      this.isI2DPlatinumItemAdded = false;
    }else if (index == 0 && !valueChanges){
      this.removePackageFromCart();
      this.isI2DGoldItemAdded = false;
    }else if(index == 1 && valueChanges){
      this.addTransferItemsToCart(items);
      this.isI2DGoldItemAdded = false;
    }else if (index == 1 && !valueChanges){
      this.removePackageFromCart();
      this.isI2DPlatinumItemAdded = false;
    }
    this.calculateGST();
  }

  togglingTransferPorterPackage(items, valueChanges){
    if(valueChanges){
      this.addTransferPortersToCart(items);
    }else if (!valueChanges){
      this.removePortersFromCart();
    }

    this.calculateGST();
  }

  togglingTransferAddonPackage(items, valueChanges){
    if(valueChanges){
      this.addTransferAddonsToCart(items);
    }else if (!valueChanges){
      this.removeAddonsFromCart();
    }
  }

  addTransferPortersToCart(values){
    this.portersOnCart = {
      "porters": values.name,
      "porter_amt": this.transferTravelType == 'd2d' ? values.amount.domestic : values.amount.international
    }
  }

  addTransferAddonsToCart(values){
    this.addonsOnCart = {
      "addons": values.name,
      "addon_amt": this.transferTravelType == 'd2d' ? values.amount.domestic : values.amount.international
    }
  }
    
  addTransferItemsToCart(values){
    this.itemsOnCart = {
      "package": values.pkgName,
      "pkg_amt": values.pkgAmount,
    }
  }


  initalValueGST(){
    this.chargeDetails = {
      "gst": 0,
      "cgst": 0,
      "sgst": 0,
      "package": "",
      "addons": "",
      "porters": "",
      "pkg_amt": 0,
      "addon_amt": 0,
      "porter_amt": 0,
      "total": 0
    } 
  }

  calculateGST(){
      this.chargeDetails = {
        "gst": (18/100) * (this.itemsOnCart.pkg_amt+this.portersOnCart.porter_amt+this.addonsOnCart.addon_amt),
        "cgst": (9/100) * (this.itemsOnCart.pkg_amt+this.portersOnCart.porter_amt+this.addonsOnCart.addon_amt),
        "sgst": (9/100) * (this.itemsOnCart.pkg_amt+this.portersOnCart.porter_amt+this.addonsOnCart.addon_amt),
        "package": this.itemsOnCart.package,
        "porters": this.portersOnCart.porters,
        "addons": this.addonsOnCart.addons,
        "pkg_amt": this.itemsOnCart.pkg_amt,
        "porter_amt": this.portersOnCart.porter_amt,
        "addon_amt": this.addonsOnCart.addon_amt,
        "total": this.itemsOnCart.pkg_amt + this.portersOnCart.porter_amt + this.addonsOnCart.addon_amt + (18/100) * (this.itemsOnCart.pkg_amt+this.portersOnCart.porter_amt+this.addonsOnCart.addon_amt)
      } 
      this.global.setCharges(this.chargeDetails);
  }


  handlePlacardName(name){
    this.placardName = name;
  }

  handlePlacardMessage(msg){
    this.placardMessage = msg;
  }

  
  goNext(){
    if(this.step1 && this.serviceName == 'departure')
    {
      if(this.isOneTimeGoldItemAdded || this.isPrevGoldItemAdded 
        || this.isOneTimePlatinumItemAdded || this.isPrevPlatinumItemAdded){
        // this.router.navigate(['departure-guest']);
        this.step1 = false;
        this.step2 = (this.isPrevPlatinumItemAdded || this.isOneTimePlatinumItemAdded) ? false : true;
        this.step3 = (this.isPrevPlatinumItemAdded || this.isOneTimePlatinumItemAdded) ? true : false;
        this.showstep3 = (this.isPrevPlatinumItemAdded || this.isOneTimePlatinumItemAdded) ? true : false;
      }
  
        else{
        bootbox.alert('Please choose atleast one package');
      }
    } else if(this.step1 && this.serviceName == 'arrival'){
      if(this.isServiceAdded){
        // this.router.navigate(['departure-guest']);
        this.step1 = false;
        this.step2 = true;
      }
        else{
        bootbox.alert('Please choose atleast one package');
      }
    }else if(this.step1 && this.serviceName == 'transfer'){
      if(this.isD2DGoldItemAdded || this.isD2DPlatinumItemAdded 
        || this.isD2IGoldItemAdded || this.isD2IPlatinumItemAdded 
        || this.isI2DGoldItemAdded || this.isI2DPlatinumItemAdded){
        // this.router.navigate(['departure-guest']);
        this.step1 = false;
        this.step2 = (this.isD2DPlatinumItemAdded || this.isD2IPlatinumItemAdded || this.isI2DPlatinumItemAdded) ? false : true;
        this.step3 = (this.isD2DPlatinumItemAdded || this.isD2IPlatinumItemAdded || this.isI2DPlatinumItemAdded) ? true : false;
        this.showstep3 = (this.isD2DPlatinumItemAdded || this.isD2IPlatinumItemAdded || this.isI2DPlatinumItemAdded) ? true : false;
        }
        else{
        bootbox.alert('Please choose atleast one package');
        }
    }
    else if(this.step2)
    {
      this.step2 = false;
      this.step3 = true;
      this.showstep3 = true;
    }
    else if(this.step3)
    {
      if(this.guestForm.valid)
      {
        this.step3 = false;
        this.showplacard = true;
      }
      else
      {
        bootbox.alert('Please enter valid details');
      }
     
    }
    else if(this.showplacard)
    {
      let placard = {
        "name": this.placardName,
        "msg": this.placardMessage
      }
  
      if(this.placardName){
        this.global.setPlacardDetails(placard);
        this.showplacard = false;
        this.showpayment = true;
      }else{
        bootbox.alert('Please enter placard name');
      }
    }
    
  }

  goBack()
  {
    if(this.showpayment)
    {
      this.showpayment = false;
      this.showplacard = true; 
    }
    else if(this.showplacard)
    {
      this.showplacard = false;
      this.step3 = true;
    }
    else if(this.step3)
    {
      this.step3 = false;  
      this.showstep3 = false;  
     this.step1 = (this.isPrevPlatinumItemAdded || this.isOneTimePlatinumItemAdded 
      || this.isD2DPlatinumItemAdded || this.isD2IPlatinumItemAdded || this.isI2DPlatinumItemAdded) ? true : false;    
      this.step2 = (this.isPrevPlatinumItemAdded || this.isOneTimePlatinumItemAdded 
        || this.isD2DPlatinumItemAdded || this.isD2IPlatinumItemAdded || this.isI2DPlatinumItemAdded) ? false : true;
    }
    else if(this.step2)
    {
      this.step2 = false;
      this.step1 = true;
    }
    else if(this.step1)
    {
      this.location.back();
    }
  }

  proceedToPay(){
    bootbox.alert("Success");
  }


}

