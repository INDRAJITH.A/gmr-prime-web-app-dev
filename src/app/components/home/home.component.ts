import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {setAnimation} from "../../../assets/js/gmr.js";
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { AngularFireDatabase } from 'angularfire2/database';
import { AuthProviderService } from '../../providers/auth-provider.service';
import { GlobalService } from '../../providers/global.service';
import { DatabaseService } from '../../providers/database.service';
import * as moment from 'moment';
declare var bootbox: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  //Departure Variables
  departureDate: any;
  selectedDepartureFlight: any;
  isDepartureFlightValid: boolean = false;
  departureFlightList: any = [];
  departureDBDate: any;
  selectedDepartureTravel: any;
  isDepartFlightAvailable: any;
  departFlightTiming: any;
  departFlightDestination: any;
  departTimeSlot: any;
  departureFlightInfoList: any;
  departureSlotOneStartDate: any;
  departureSlotTwoStartDate: any;
  departureSlotTwoEndDate: any;

  //Arrival Variables
  arrivalDate: any;
  selectedArrivalFlight: any;
  isArrivalFlightValid: boolean = false;
  arrivalFlightList: any = [];
  arrivalDBDate: any;
  selectedArrivalTravel: any;
  isArrivalFlightAvailable: any;
  arrivalFlightTiming: any;
  arrivalFlightDestination: any;
  arrivalTimeSlot: any;
  arrivalFlightInfoList: any;
  arrivalSlotOneStartDate: any;
  arrivalSlotTwoStartDate: any;
  arrivalSlotTwoEndDate: any;

  //Transfer Variables
  selectedTransferTravel: any;
  transferADate: any;
  transferDDate: any;

  //Common Variables
  userObj: any;
  slotMinDate: any;
  slotMaxDate: any;
  selectedLanguage: any;
  selectedService: any; 
  infantCounts: any;
  adultCounts: any;
  childCounts: any;
  visibleDepartForm: boolean = true;
  visibleDepartForm2: boolean = false;
  visibleArrivalForm: boolean = true;
  visibleArrivalForm2: boolean = false;
  visibleTransForm: boolean = true;
  visibleTransferForm2: boolean = false;

  

  constructor(
    public authService: AuthProviderService,
    public firebaseDB: AngularFireDatabase,
    public router: Router,
    public global: GlobalService,
    public database: DatabaseService) { }

    
  ngOnInit() {
    setAnimation();
    this.global.clearLocalStorage();
    this.selectedService = 'departure';
    this.global.setServiceName('departure');
    this.selectedLanguage = 'en';
    this.infantCounts = 0;
    this.global.setInfantCount(0);
    this.adultCounts = 1;
    this.global.setAdultCount(1);
    this.childCounts = 0;
    this.global.setChildCount(0);
    this.selectedTransferTravel = 'd2d';
    this.global.setTransferTravelType('d2d');
    this.selectedArrivalTravel = 'domestic';
    this.global.setArrivalTravelType('domestic');
    this.selectedDepartureTravel = 'domestic';
    this.global.setDepartTravelType('domestic');
    this.slotMinDate = new Date();
    this.slotMaxDate = this.getMaxdate();
  }

  getMaxdate(){
    let endDate = moment("31/03/2019", "DD/MM/YYYY");
    let daysDiff = endDate.diff(new Date(), 'days');

    if(daysDiff > 90){
     return this.slotMaxDate = moment(new Date()).add(moment.duration(90, 'days')).toDate();
    }else{
     return this.slotMaxDate = new Date("2019-03-31");
    }
  }

  //// GOOGLE ////
  async googleLogin() {
      this.authService.webGoogleLogin()
        .then(response => {
          this._toSaveFirebaseWeb(response["user"]);
        }).catch(error => {});
  }

  //// FACEBOOK ////
  async facebookLogin() {
      this.authService.webFacebookLogin()
        .then(response => {
          this._toSaveFirebaseWeb(response["user"]);
        }).catch(error => {});
  }

  //// TWITTER ////
  async twitterLogin() {
      this.authService.webTwitterLogin()
      .then(response => {
        this._toSaveFirebaseWeb(response["user"]);
      }).catch(error => {});
  }

  //// STORE USER DATA ////
  _toSaveFirebaseWeb(userObj) {
    this.database.newUserRegister(userObj.uid).subscribe(res => {
      this.userObj = res;
      if (this.userObj == null) {
        var postData = {
          "profile": {
            "createdAt": new Date().getTime(),
            "name": userObj.displayName ? userObj.displayName : "",
            "phoneno": userObj.phoneNumber ? userObj.phoneNumber.toString().substring(3) : "",
            "country": "",
            "state": "",
            "email": userObj.email ? userObj.email : "",
            "profileImg": userObj.photoURL ? userObj.photoURL : "../../../assets/images/home/profile.png"
          }
        }
        this.database.newUserRegisterUpdate(userObj.uid, postData);
      }
    });
  }

  //// DEPARTURE SERVICE ////
  handleDepartureTravelType(travel_type){
    this.selectedDepartureFlight = "";
    this.isDepartureFlightValid = false;
    this.selectedDepartureTravel = travel_type;
    this.global.setDepartTravelType(travel_type);
  }

  //// DEPARTURE SERVICE ////
  handleDepartureDate(value){
    let endDate = moment(new Date(value), "DD/MM/YYYY");
    let hoursDiff = endDate.diff(new Date(), 'hours');
    let arrivalDate = endDate.diff(this.arrivalDate, 'days');
    this.selectedDepartureFlight = "";
    this.isDepartureFlightValid = false;

    if(hoursDiff < 24){
      bootbox.alert("Flight time should not be less than 24 hours");
      this.departureDate = "";
      this.departureDBDate = "";
      this.departureFlightList = [];
    }else if(arrivalDate <= 0){
      bootbox.alert("Departure flight date should not be less than arrival flight");
    }else if(hoursDiff >= 24){
      this.departureDate = value;
      this.departureDBDate = this.convertToDateFormat(value, 'departure');
    }
  }

  //// DEPARTURE SERVICE ////
  getDepartureFlightList(){
    this.selectedService = 'departure';
    if(this.selectedDepartureTravel && this.departureDBDate){
      this.database.flightNumbers(this.selectedService + "/" + this.selectedDepartureTravel + "/" + this.departureDBDate).subscribe(res => {
         this.departureFlightList = res;
      });
     }else if(this.selectedTransferTravel && this.departureDBDate){
      if((this.selectedTransferTravel == 'd2d' || this.selectedTransferTravel == 'd2i')){
        this.selectedDepartureTravel = 'domestic';
        this.database.flightNumbers(this.selectedService + "/" + this.selectedDepartureTravel+ "/" +this.departureDBDate).subscribe(res => {
          this.departureFlightList = res;
        });
      }else if(this.selectedTransferTravel == 'i2d'){
        this.selectedDepartureTravel = 'international';
        this.database.flightNumbers(this.selectedService + "/" + this.selectedDepartureTravel+ "/" + this.departureDBDate).subscribe(res => {
          this.departureFlightList = res;
        });
      }
    }
  }

  //// DEPARTURE SERVICE ////
  getDepartFlightInfoList(flight_no){
    this.database.singleFlightInfo(this.selectedService + "/" + this.selectedDepartureTravel + "/" + this.departureDBDate + "/" +flight_no).subscribe(res => {
      if(res){
        this.isDepartFlightAvailable = true;
        this.departureFlightInfoList = res;
        this.departFlightTiming = this.departureFlightInfoList.mt;
        this.departFlightDestination = this.departureFlightInfoList.dest;
        this.global.setDepartFlightTime(this.departFlightTiming);
        this.global.setDepartDestination(this.departFlightDestination);
        this.global.setDepartFlightNo(this.departureFlightInfoList.flNo);
        this.global.setDepartAirline(this.departureFlightInfoList.airlineInfo.airLine);

        this.departureSlotOneStartDate = moment("1/1/1970 "+this.departFlightTiming, "DD/MM/YYYY hh:mm A").subtract(moment.duration(this.selectedDepartureTravel == 'domestic' ? 3: 4, 'hours')).format("hh:mm A");
        this.departureSlotTwoStartDate = moment("1/1/1970 "+this.departFlightTiming, "DD/MM/YYYY hh:mm A").subtract(moment.duration(this.selectedDepartureTravel == 'domestic' ? 2: 3, 'hours')).format("hh:mm A");
        this.departureSlotTwoEndDate = moment("1/1/1970 "+this.departFlightTiming, "DD/MM/YYYY hh:mm A").subtract(moment.duration(this.selectedDepartureTravel == 'domestic' ? 1: 2, 'hours')).format("hh:mm A");      
      } else{
        this.isDepartFlightAvailable = false;
      } 
    });
  }

  //// DEPARTURE SERVICE ////
  onSelectDepartureFlight(event: TypeaheadMatch){
    this.selectedDepartureFlight = event.item;
    this.isDepartureFlightValid = true;
    this.getDepartFlightInfoList(event.item);
  }

  //// DEPARTURE SERVICE ////
  departNextForm(){
    if(!this.selectedDepartureTravel){
      bootbox.alert("Please select the travel type");
    }else if(!this.departureDBDate){
      bootbox.alert("Please select the departure date");
    }else if(!this.isDepartureFlightValid){
      bootbox.alert("Please select the flight number");
    }else if(!this.isDepartFlightAvailable){
      bootbox.alert("Flight not available for the selected date");
    }else{
      this.visibleSecondaryForm();
    }
  }

  //// DEPARTURE FUNCTION ////
  goDepartureService(){
    if(!this.departTimeSlot){
      bootbox.alert("Select the time slot");
    }else if(this.adultCounts <=0 && this.childCounts <=0){
      bootbox.alert("Select atleast one adult or child");
    }else if(!this.selectedLanguage){
      bootbox.alert("Select the language");
    }else{
      this.router.navigate(['booking-service']);
    }
  }

  //// ARRIVAL SERVICE ////
  handleArrivalTravelType(travel_type){
    this.selectedArrivalFlight = "";
    this.isArrivalFlightValid = false;
    this.selectedArrivalTravel = travel_type;
    this.global.setArrivalTravelType(travel_type);
  }

  //// ARRIVAL SERVICE ////
  handleArrivalDate(value: Date){
    let endDate = moment(new Date(value), "DD/MM/YYYY");
    let hoursDiff = endDate.diff(new Date(), 'hours');
    this.selectedArrivalFlight = "";
    this.isArrivalFlightValid = false;
    if(hoursDiff < 24){
      bootbox.alert("Flight time should not be less than 24 hours");
      this.arrivalDate = "";
      this.arrivalDBDate = "";
      this.arrivalFlightList = [];
    }else if(hoursDiff >= 24){
      this.arrivalDate = value;
      this.arrivalDBDate = this.convertToDateFormat(value, 'arrival');
    }
  }

  //// ARRIVAL SERVICE ////
  getArrivalFlightList(){
    this.selectedService = 'arrival';
    if(this.selectedArrivalTravel && this.arrivalDBDate){
      this.database.flightNumbers(this.selectedService + "/" + this.selectedArrivalTravel + "/" + this.arrivalDBDate).subscribe(res => {
        this.arrivalFlightList = res;
      });
    }else if(this.selectedTransferTravel && this.arrivalDBDate){
      if((this.selectedTransferTravel == 'd2d' || this.selectedTransferTravel == 'd2i')){
        this.selectedArrivalTravel = 'domestic';
        this.database.flightNumbers(this.selectedService + "/" + this.selectedArrivalTravel + "/" +this.arrivalDBDate).subscribe(res => {
          this.arrivalFlightList = res;
        });
      }else if(this.selectedTransferTravel == 'i2d'){
        this.selectedArrivalTravel = 'international';
        this.database.flightNumbers(this.selectedService + "/" + this.selectedArrivalTravel + "/" + this.arrivalDBDate).subscribe(res => {
          this.arrivalFlightList = res;
        });
      }
    }
  }

  //// ARRIVAL SERVICE ////
  getArrivalFlightInfoList(flight_no){
    this.database.singleFlightInfo(this.selectedService + "/" + this.selectedArrivalTravel + "/" + this.arrivalDBDate + "/" +flight_no).subscribe(res => {
      if(res){
        this.isArrivalFlightAvailable = true;
        this.arrivalFlightInfoList = res;
        this.arrivalFlightTiming = this.arrivalFlightInfoList.mt;
        this.arrivalFlightDestination = this.arrivalFlightInfoList.origin;
        this.global.setArrivalFlightTime(this.arrivalFlightTiming);
        this.global.setArrivalDestination(this.arrivalFlightDestination);
        this.global.setArrivalFlightNo(this.arrivalFlightInfoList.flNo);
        this.global.setArrivalAirline(this.arrivalFlightInfoList.airlineInfo.airLine);

        this.arrivalSlotOneStartDate = moment("1/1/1970 "+this.arrivalFlightTiming, "DD/MM/YYYY hh:mm A").add(moment.duration(15, 'minutes')).format("hh:mm A");
        this.arrivalSlotTwoStartDate = moment("1/1/1970 "+this.arrivalFlightTiming, "DD/MM/YYYY hh:mm A").add(moment.duration(45, 'minutes')).format("hh:mm A");
        this.arrivalSlotTwoEndDate = moment("1/1/1970 "+this.arrivalFlightTiming, "DD/MM/YYYY hh:mm A").add(moment.duration(75, 'minutes')).format("hh:mm A");      
      } else{
        this.isArrivalFlightAvailable = false;
      } 
    });
  }

  //// ARRIVAL SERVICE ////
  onSelectArrivalFlight(event: TypeaheadMatch){
    this.selectedArrivalFlight = event.item;
    this.isArrivalFlightValid = true;
    this.getArrivalFlightInfoList(event.item);
  }

  //// ARRIVAL SERVICE ////
  arrivalNextForm(){
    if(!this.selectedArrivalTravel){
      bootbox.alert("Please select the travel type");
    }else if(!this.arrivalDBDate){
      bootbox.alert("Please select the flight arrival date");
    }else if(!this.isArrivalFlightValid){
      bootbox.alert("Please select the flight number");
    }else if(!this.isArrivalFlightAvailable){
      bootbox.alert("Flight not available for the selected date");
    }else{
      this.visibleSecondaryForm();
    }
  }

  //// ARRIVAL FUNCTION ////
  goArrivalService(){
    if(!this.arrivalTimeSlot){
      bootbox.alert("Select the time slot");
    }else if(this.adultCounts <=0 && this.childCounts <=0){
      bootbox.alert("Select the number of passengers");
    }else if(!this.selectedLanguage){
      bootbox.alert("Select the language");
    }else{
      this.router.navigate(['booking-service']);
    }
  }

  //// TRANSFER FUNCTION ////
  handleTransferTravelType(travel_type){
    this.selectedTransferTravel = travel_type;
    this.global.setTransferTravelType(travel_type);
  }

  //// TRANSFER FUNCTION ////
  goNextFromTransfer(){
    if(!this.selectedTransferTravel){
      bootbox.alert("Please select travel type");
   }else if(!this.arrivalDBDate){
      bootbox.alert("Please select flight arrival date");
   }else if(!this.selectedArrivalFlight){
      bootbox.alert("Please select valid arrival flight number");
   }else if(!this.isArrivalFlightValid){
      bootbox.alert("Please select valid arrival flight number");
   }else if(!this.isArrivalFlightAvailable){
      bootbox.alert("Arrival flight not available");
   }else if(!this.departureDBDate){
      bootbox.alert("Please select flight departure date");   
   }else if(!this.selectedDepartureFlight){
      bootbox.alert("Please select valid departure flight number");
   }else if(!this.isDepartureFlightValid){
      bootbox.alert("Please select valid departure flight number");
   }else if(!this.isDepartFlightAvailable){
      bootbox.alert("Departure flight not available");
   }
  //  else if(this.selectedTransferTravel == 'd2d' && (this.transferTimeDiff() < 1 || this.transferTimeDiff() > 6)){
  //     bootbox.alert("Time differnce between arrival and departure is more or less than "+ this.transferTimeDiff()+" Hours");
  //  }else if((this.selectedTransferTravel == 'd2i' || this.selectedTransferTravel == 'i2d') && (this.transferTimeDiff() < 2 || this.transferTimeDiff() > 12)){
  //   bootbox.alert("Time differnce between arrival and departure is more or less than "+ this.transferTimeDiff()+" Hours");
  //  }
   else{
      this.visibleSecondaryForm();
   }
  }

  //// TRANSFER FUNCTION ////
  goTranserService(){
    if(!this.arrivalTimeSlot){
      bootbox.alert("Select the  arrival time slot");
    }else if(!this.departTimeSlot){
      bootbox.alert("Select the  departure time slot");
    }else if(this.adultCounts <=0 && this.childCounts <=0){
      bootbox.alert("Select the number of passengers");
    }else if(!this.selectedLanguage){
      bootbox.alert("Select the language");
    }else{
      this.router.navigate(['booking-service']);
    }
  }

  //// TRANSFER FUNCTION ////
  transferTimeDiff(){
    let arrival_datetime = moment(this.transferADate+" "+this.arrivalFlightTiming, "DD/MM/YYYY hh:mm A");
    let departure_datetime = moment(this.transferDDate+" "+this.departFlightTiming, "DD/MM/YYYY hh:mm A");
    return Math.abs(moment.duration(departure_datetime.diff(arrival_datetime)).asHours());
  }

  //// SECOND FORM FUNCTION ////
  onDepartureTimeSlotChange(time_slot){
    this.departTimeSlot = time_slot;
    if(time_slot == "slot1"){
      this.global.setDepartServiceTime(this.departureSlotOneStartDate)
    }else{
     this.global.setDepartServiceTime(this.departureSlotTwoStartDate)
    }
  }

  //// SECOND FORM FUNCTION ////
  onArrivalTimeSlotChange(time_slot){
    this.arrivalTimeSlot = time_slot;
    if(time_slot == "slot1"){
      this.global.setArrivalServiceTime(this.arrivalSlotOneStartDate)
    }else{
     this.global.setArrivalServiceTime(this.arrivalSlotTwoStartDate)
    }
  }
  
  //// SECOND FORM FUNCTION ////
  handleAdultCounts(adults){
    this.adultCounts = adults;
    this.global.setAdultCount(adults);
  }

  //// SECOND FORM FUNCTION ////
  handleChildCounts(childs){
    this.childCounts = childs;
    this.global.setChildCount(childs);
  }

   //// SECOND FORM FUNCTION ////
   handleInfantCounts(infants){
    this.infantCounts = infants;
    this.global.setInfantCount(infants);
  }

  //// SECOND FORM FUNCTION ////
  handleChoosenLanguage(lang){
    this.selectedLanguage = lang;
  }


  //// COMMON FUNCTION ////
  convertToDateFormat(value, service){
    let date = (value.getDate().toString().length == 1) ? "0"+value.getDate() : value.getDate();
    let actual_month = value.getMonth()+1;
    let month = (actual_month.toString().length == 1) ? "0"+actual_month : actual_month;
    let year = value.getFullYear();

    if(service == 'departure'){
      this.transferDDate = moment(new Date(year, month-1, date)).format('DD/MM/YYYY');
      this.global.setDepartFlightDate(moment(new Date(year, month-1, date)).format('D MMM, YYYY'));
    }else if(service == 'arrival'){
      this.transferADate = moment(new Date(year, month-1, date)).format('DD/MM/YYYY');
      this.global.setArrivalFlightDate(moment(new Date(year, month-1, date)).format('D MMM, YYYY'));
    }
    return year+""+month+""+date;
  }

  //// COMMON FUNCTION ////
  chooseService(service: String){
    this.selectedService = service;
    this.global.setServiceName(service);
    this.departureDate = "";
    this.selectedDepartureFlight = "";
    this.selectedDepartureTravel = 'domestic';
    this.arrivalDate = "";
    this.selectedArrivalFlight = "";
    this.selectedArrivalTravel = 'domestic';
    this.visiblePrimaryForm();
  }

  //// COMMON FUNCTION ////
  visiblePrimaryForm(){
    this.visibleDepartForm =  true;
    this.visibleArrivalForm =  true;
    this.visibleTransForm =  true;  
    this.visibleDepartForm2 = false;
    this.visibleArrivalForm2 = false;  
    this.visibleTransferForm2 = false;
  }

  //// COMMON FUNCTION ////
  visibleSecondaryForm(){
    this.visibleDepartForm =  false;
    this.visibleArrivalForm =  false;
    this.visibleTransForm =  false;  
    this.visibleDepartForm2 = true;
    this.visibleArrivalForm2 = true;  
    this.visibleTransferForm2 = true;
  }
  
  //// COMMON FUNCTION ////
  goBack(){
    this.visibleDepartForm2 = false; 
    this.visibleDepartForm = true;
    this.visibleArrivalForm2 = false; 
    this.visibleArrivalForm = true;
    this.visibleTransferForm2 = false;
    this.visibleTransForm = true;
  }
}
