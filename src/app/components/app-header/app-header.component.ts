import { Component, OnInit } from '@angular/core';
// import { $ } from 'protractor';
declare var $: any;
@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  _toSignIn(){
    $(".singUP").slideToggle();
  }

}
