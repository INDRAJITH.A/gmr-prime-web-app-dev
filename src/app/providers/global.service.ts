import { Injectable } from '@angular/core';
import { servicesVersion } from 'typescript';

@Injectable()
export class GlobalService {

  constructor() { }

  //// DEPART STORAGE ////
  setDepartTravelType(depart_travel){
    localStorage.setItem("departTravelType", depart_travel);
  }
  getDepartTravelType(){
    return localStorage.getItem("departTravelType");
  }


  setDepartFlightTime(depart_flight_time){
    localStorage.setItem("departFlightTime", depart_flight_time);
  }
  getDepartFlightTime(){
    return localStorage.getItem("departFlightTime");
  }


  setDepartDestination(depart_dest){
    localStorage.setItem("departDestination", depart_dest);
  }
  getDepartDestination(){
    return localStorage.getItem("departDestination");
  }

  
  setDepartFlightNo(depart_flight_no){
    localStorage.setItem("departFlightNo", depart_flight_no);
  }
  getDepartFlightNo(){
    return localStorage.getItem("departFlightNo");
  }


  setDepartAirline(depart_airline){
    localStorage.setItem("departAirLine", depart_airline);
  }
  getDepartAirline(){
    return localStorage.getItem("departAirLine");
  }

 
  setDepartFlightDate(depart_flight_date){
    localStorage.setItem("departFlightDate", depart_flight_date);
  }
  getDepartFlightDate(){
    return localStorage.getItem("departFlightDate");
  }

  
  setDepartServiceTime(depart_service){
    localStorage.setItem("departServiceTime", depart_service);
  }
  getDepartServiceTime(){
    return localStorage.getItem("departServiceTime");
  }





    //// ARRIVAL STORAGE ////
    setArrivalTravelType(depart_travel){
      localStorage.setItem("arrivalTravelType", depart_travel);
    }
    getArrivalTravelType(){
      return localStorage.getItem("arrivalTravelType");
    }
  
  
    setArrivalFlightTime(depart_flight_time){
      localStorage.setItem("arrivalFlightTime", depart_flight_time);
    }
    getArrivalFlightTime(){
      return localStorage.getItem("arrivalFlightTime");
    }
  
  
    setArrivalDestination(depart_dest){
      localStorage.setItem("arrivalDestination", depart_dest);
    }
    getArrivalDestination(){
      return localStorage.getItem("arrivalDestination");
    }
  
    
    setArrivalFlightNo(depart_flight_no){
      localStorage.setItem("arrivalFlightNo", depart_flight_no);
    }
    getArrivalFlightNo(){
      return localStorage.getItem("arrivalFlightNo");
    }
  
  
    setArrivalAirline(depart_airline){
      localStorage.setItem("arrivalAirLine", depart_airline);
    }
    getArrivalAirline(){
      return localStorage.getItem("arrivalAirLine");
    }
  
   
    setArrivalFlightDate(depart_flight_date){
      localStorage.setItem("arrivalFlightDate", depart_flight_date);
    }
    getArrivalFlightDate(){
      return localStorage.getItem("arrivalFlightDate");
    }
  

    setArrivalServiceTime(depart_service){
      localStorage.setItem("arrivalServiceTime", depart_service);
    }
    getArrivalServiceTime(){
      return localStorage.getItem("arrivalServiceTime");
    }


    
  //// TRANSFER STORAGE ////
  setTransferTravelType(transfer_travel){
    localStorage.setItem("transferTravelType", transfer_travel);
  }
  getTransferTravelType(){
    return localStorage.getItem("transferTravelType");
  }



  //// COMMON FOR ALL ////
  setServiceName(service){
    localStorage.setItem("serviceName", service);
  }
  getServiceName(){
    return localStorage.getItem("serviceName");
  }

  setAdultCount(adult){
    localStorage.setItem("adultCount", adult);
  }
  getAdultCount(){
    return localStorage.getItem("adultCount");
  }

  setInfantCount(adult){
    localStorage.setItem("infantCount", adult);
  }
  getInfantCount(){
    return localStorage.getItem("infantCount");
  }

  setChildCount(child){
    localStorage.setItem("childCount", child);
  }
  getChildCount(){
    return localStorage.getItem("childCount");
  }

  setCharges(amount){
    localStorage.setItem("charges", JSON.stringify(amount));
  }
  getCharges(){
    return JSON.parse(localStorage.getItem("charges"));
  }

  setPlacardDetails(details){
    localStorage.setItem("placard", JSON.stringify(details));    
  }
  getPlacardDetails(){
    return JSON.parse(localStorage.getItem("placard"));
  }

  clearLocalStorage(){
    localStorage.clear();
  }

}
