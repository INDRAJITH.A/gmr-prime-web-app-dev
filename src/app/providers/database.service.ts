import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { GlobalService } from '../providers/global.service';

@Injectable()
export class DatabaseService {

  constructor(
    public firebaseDB: AngularFireDatabase,
    public global: GlobalService) { }


    newUserRegister(data){
      return this.firebaseDB.object('userDetails/'+data).valueChanges();
    }

    newUserRegisterUpdate(data, data2){
      this.firebaseDB.object('userDetails/'+data).update(data2).then(function () {});
    }


    flightNumbers(data){
      return this.firebaseDB.object('/flightInfo/'+data+"/flList").valueChanges();
    }

    singleFlightInfo(data){
      return this.firebaseDB.object('/flightInfo/'+data).valueChanges();
    }
   

}
