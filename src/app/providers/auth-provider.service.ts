import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';


@Injectable()
export class AuthProviderService {
  
  constructor(
    public afAuth: AngularFireAuth) {}


  /// GOOGLE : Web
  async webGoogleLogin(){
    try {
      const provider = new firebase.auth.GoogleAuthProvider();
      return await this.afAuth.auth.signInWithPopup(provider);
    } catch (err) {
      console.log(err)
    }
  }

  //// FACEBOOK : Web
  async webFacebookLogin(){
    try {
      const provider = new firebase.auth.FacebookAuthProvider();
      return await this.afAuth.auth.signInWithPopup(provider);
    } catch (err) {
      console.log(err);
    }
  }

  async webTwitterLogin(){
    try {
      const provider = new firebase.auth.TwitterAuthProvider();
      return await this.afAuth.auth.signInWithPopup(provider);
    } catch (err) {
      console.log(err);
    }
  }
}
