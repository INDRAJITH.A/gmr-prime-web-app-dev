import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { AuthProviderService } from '../app/providers/auth-provider.service';
import { GlobalService } from '../app/providers/global.service';
import { DatabaseService } from '../app/providers/database.service';

import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ServiceComponent } from './components/service/service.component';
import { ManageBookingsComponent } from './components/manage-bookings/manage-bookings.component';
import { FlightStatusComponent } from './components/flight-status/flight-status.component';
import { ContactComponent } from './components/contact/contact.component';
import { AppHeaderComponent } from './components/app-header/app-header.component';
import { BookingServiceComponent } from './components/booking-service/booking-service.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'booking-service', component: BookingServiceComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BookingServiceComponent,
    AboutComponent,
    ServiceComponent,
    ManageBookingsComponent,
    FlightStatusComponent,
    ContactComponent,
    AppHeaderComponent
  ],
  imports: [
    BrowserModule,
    TypeaheadModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule.forRoot(routes)
  ],
  providers: [
    AngularFireAuth,
    AngularFireDatabase,
    AuthProviderService,
    GlobalService,
    DatabaseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
